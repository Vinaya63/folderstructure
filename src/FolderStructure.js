import React, { Component } from 'react';
import { Treebeard } from 'react-treebeard';
import './FolderStructure.css';

class FolderStructure extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data, // JSON tree with leaf nodes as an array
      activeNode: null
    };
  }

  onToggle = (node, toggled) => {
    const { data, activeNode } = this.state;
    if (activeNode) {
      activeNode.active = false;
    }
    node.active = true;
    if (node.children) {
      node.toggled = toggled;
    }
    this.setState({ data, activeNode: node });
  };

  addFolder = () => {
    const { activeNode } = this.state;
    const nodeName = prompt('Enter folder name:');
    if (nodeName) {
      const node = {
        name: nodeName,
        toggled: true,
        children: []
      };
      if (activeNode) {
        activeNode.children.push(node);
      } else {
        this.state.data.children.push(node);
      }
      this.setState({ data: this.state.data });
    }
  };

  addFile = () => {
    const { activeNode } = this.state;
    const nodeName = prompt('Enter file name:');
    if (nodeName) {
      const node = {
        name: nodeName,
        type: 'file'
      };
      if (activeNode) {
        activeNode.children.push(node);
      } else {
        this.state.data.children.push(node);
      }
      this.setState({ data: this.state.data });
    }
  };

  renameNode = () => {
    const { activeNode } = this.state;
    const nodeName = prompt('Enter new name:', activeNode.name);
    if (nodeName) {
      activeNode.name = nodeName;
      this.setState({ data: this.state.data });
    }
  };

  deleteNode = () => {
    const { activeNode } = this.state;
    if (activeNode) {
      const { data } = this.state;
      const parent = this.findParent(data, activeNode);
      if (parent) {
        parent.children.splice(parent.children.indexOf(activeNode), 1);
        this.setState({ data: this.state.data, activeNode: null });
      } else {
        this.setState({ data: { children: [] }, activeNode: null });
      }
    }
  };

  findParent = (node, child) => {
    if (node.children && node.children.indexOf(child) > -1) {
      return node;
    } else if (node.children) {
      for (let i = 0; i < node.children.length; i++) {
        const parent = this.findParent(node.children[i], child);
        if (parent) {
          return parent;
        }
      }
    }
  };

  render() {
    return (
      <>
        <div>
          <button onClick={this.addFolder}>Add Folder</button>
          <button onClick={this.addFile}>Add File</button>
          <button onClick={this.renameNode}>Rename</button>
          <button onClick={this.deleteNode}>Delete</button>
        </div>
        <Treebeard
          data={this.state.data}
          onToggle={this.onToggle}
          className="treebeard treebeard-custom"
        />
      </>
    );
  }
}

export default FolderStructure;