import React from 'react';
import FolderStructure from './FolderStructure';

const data = {
  name: 'root',
  toggled: true,
  children: [
    {
      name: 'Documents',
      children: [
        { name: 'Document1.jpg', type: 'file' },
        { name: 'Document2.jpg', type: 'file' },
        { name: 'Document3.jpg', type: 'file' }
      ]
    },
    {
      name: 'Desktop',
      children: [
        { name: 'Screenshot1.jpg', type: 'file' },
        { name: 'videopal.mp4', type: 'file' }
      ]
    },
    {
      name: 'Downloads',
      children: [
        {
          name: 'Drivers',
          children: [
            { name: 'Printerdriver.dmg', type: 'file' },
            { name: 'cameradriver.dmg', type: 'file' }
          ]
        },
        {
          name: 'Applications',
          children: [
            { name: 'Webstorm.dmg', type: 'file' },
            { name: 'Pycharm.dmg', type: 'file' },
            { name: 'FileZila.dmg', type: 'file' },
            { name: 'Mattermost.dmg', type: 'file' }
          ]
        },
        { name: 'chromedriver.dmg', type: 'file' }
      ]
    }
  ]
};

function App() {
  return (
    <div>
      <FolderStructure data={data} />
    </div>
  );
}

export default App;